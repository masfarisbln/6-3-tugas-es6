// Soal 1
console.log("Soal 1");
const hitungLuas = (panjang, lebar) => {
    return panjang * lebar;
}

const hitungKeliling = (panjang, lebar) => {
    return 2 * (panjang + lebar);
}

console.log("Luas persegi panjang " + hitungLuas(10,20));
console.log("Keliling persegi panjang " + hitungKeliling(10,20) );

// Soal 2
console.log( );
console.log("Soal 2");

const newFunction = (firstName, lastName) => {
    return firstName + " " + lastName;
}

console.log(newFunction("William", "Imoh"));

// Soal 3
console.log( );
console.log("Soal 3");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};
  
const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

// Soal 4
console.log( );
console.log("Soal 4");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined);

// Soal 5
console.log( );
console.log("Soal 5");

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(before);
